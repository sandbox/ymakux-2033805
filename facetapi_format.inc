<?php

class FacetapiFilterFormatItems extends FacetapiFilter {

  /**
   * Filters facet items.
   */
  public function execute(array $build) {
    $type = $this->settings->settings['format_type'];
    if ($info = $this->getFormatTypes($type)) {
      $function = NULL;
      if (!empty($info['callback'])) {
        $function = $info['callback'];
      }
      elseif (!empty($info['base'])) {
        $function = $info['base'] . '_' . $type;
      }
      
      if ($function && function_exists($function)) {
        $function($build, $this->settings->settings);
      }
    }
    return $build;
  }

  /**
   * Adds configuration form
   */
  function settingsForm(&$form, &$form_state) {

    $type = $this->settings->settings['format_type'];
    $form_state['format_item_types'] = $types = $this->getFormatTypes();

    $options = array();
    foreach ($types as $type_name => $type_data) {
      $options[$type_name] = check_plain($type_data['label']);
    }

    $form['format_type'] = array(
      '#title' => t('Format type'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $type,
    );
    
    if (!empty($types[$type])) {
      if (!empty($types[$type]['settings_form']) && function_exists($types[$type]['settings_form'])) {
        $function = $types[$type]['settings_form'];
        $function($form, $form_state, $this->settings->settings);
      }
      /*
      // TODO Validate form. The code below doesn't work
      if (!empty($types[$type]['settings_validate']) && function_exists($types[$type]['settings_validate'])) {
        $form['#validate'][] = $types[$type]['settings_validate'];
      }
      */
    }
  }

  /**
   * Default values for settings form
   */
  function getDefaultSettings() {
    $dafaults = array('format_type' => NULL);
    foreach ($this->getFormatTypes() as $type_name => $type_data) {
      if (!empty($type_data['settings_form']) && function_exists($type_data['settings_form'])) {
        $form = $form_state = $settings = array();
        $function = $type_data['settings_form'];
        $function($form, $form_state, $settings);
        foreach ($form as $key => $value) {
          $dafaults[$key] = isset($value['#default_value']) ? $value['#default_value'] : NULL;
        }
      }
    }
    return $dafaults;
  }

  private function getFormatTypes($type = NULL) {
    return facetapi_format_item_types_get($type);
  }
}
